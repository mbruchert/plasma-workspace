# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
# Guðmundur Erlingsson <gudmundure@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-08 02:24+0000\n"
"PO-Revision-Date: 2022-10-19 00:04+0000\n"
"Last-Translator: Guðmundur Erlingsson <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.3\n"

#: package/contents/config/config.qml:19
#, kde-format
msgid "Appearance"
msgstr "Útlit"

#: package/contents/config/config.qml:24
#, kde-format
msgid "Calendar"
msgstr "Dagatal"

#: package/contents/config/config.qml:29
#: package/contents/ui/CalendarView.qml:431
#, kde-format
msgid "Time Zones"
msgstr "Tímabelti"

#: package/contents/ui/CalendarView.qml:110
#, kde-format
msgid "Events"
msgstr "Viðburðir"

#: package/contents/ui/CalendarView.qml:118
#, kde-format
msgctxt "@action:button Add event"
msgid "Add…"
msgstr "Bæta við…"

#: package/contents/ui/CalendarView.qml:122
#, kde-format
msgctxt "@info:tooltip"
msgid "Add a new event"
msgstr "Bæta við nýjum viðburði"

#: package/contents/ui/CalendarView.qml:397
#, kde-format
msgid "No events for today"
msgstr "Engir viðburðir í dag"

#: package/contents/ui/CalendarView.qml:398
#, kde-format
msgid "No events for this day"
msgstr "Engir viðburðir á þessum degi"

#: package/contents/ui/CalendarView.qml:440
#, kde-format
msgid "Switch…"
msgstr "Skipta…"

#: package/contents/ui/CalendarView.qml:441
#: package/contents/ui/CalendarView.qml:444
#, kde-format
msgid "Switch to another timezone"
msgstr "Skipta yfir á annað tímabelti"

#: package/contents/ui/configAppearance.qml:51
#, kde-format
msgid "Information:"
msgstr "Upplýsingar:"

#: package/contents/ui/configAppearance.qml:55
#, kde-format
msgid "Show date"
msgstr "Sýna dagsetningu"

#: package/contents/ui/configAppearance.qml:63
#, kde-format
msgid "Adaptive location"
msgstr "Aðlaganleg staðsetning"

#: package/contents/ui/configAppearance.qml:64
#, kde-format
msgid "Always beside time"
msgstr "Alltaf við hlið tíma"

#: package/contents/ui/configAppearance.qml:65
#, kde-format
msgid "Always below time"
msgstr "Alltaf undir tíma"

#: package/contents/ui/configAppearance.qml:73
#, fuzzy, kde-format
#| msgid "Show seconds"
msgid "Show seconds:"
msgstr "Sýna sekúndur"

#: package/contents/ui/configAppearance.qml:75
#, kde-format
msgctxt "@option:check"
msgid "Never"
msgstr ""

#: package/contents/ui/configAppearance.qml:76
#, kde-format
msgctxt "@option:check"
msgid "Only in the tooltip"
msgstr ""

#: package/contents/ui/configAppearance.qml:77
#: package/contents/ui/configAppearance.qml:97
#, kde-format
msgid "Always"
msgstr "Alltaf"

#: package/contents/ui/configAppearance.qml:87
#, kde-format
msgid "Show time zone:"
msgstr "Sýna tímabelti:"

#: package/contents/ui/configAppearance.qml:92
#, kde-format
msgid "Only when different from local time zone"
msgstr "Aðeins þegar er annað en í staðartímabelti"

#: package/contents/ui/configAppearance.qml:106
#, kde-format
msgid "Display time zone as:"
msgstr "Sýna tímabelti sem:"

#: package/contents/ui/configAppearance.qml:111
#, kde-format
msgid "Code"
msgstr "Kóði"

#: package/contents/ui/configAppearance.qml:112
#, kde-format
msgid "City"
msgstr "Borg/Sveitarfélag"

#: package/contents/ui/configAppearance.qml:113
#, kde-format
msgid "Offset from UTC time"
msgstr "Hliðrun frá UTC-tíma"

#: package/contents/ui/configAppearance.qml:125
#, kde-format
msgid "Time display:"
msgstr "Birting tíma:"

#: package/contents/ui/configAppearance.qml:130
#, kde-format
msgid "12-Hour"
msgstr "12-klukkustunda"

#: package/contents/ui/configAppearance.qml:131
#: package/contents/ui/configCalendar.qml:53
#, kde-format
msgid "Use Region Defaults"
msgstr "Nota sjálfgildi fyrir landssvæðið"

#: package/contents/ui/configAppearance.qml:132
#, kde-format
msgid "24-Hour"
msgstr "24-klukkustunda"

#: package/contents/ui/configAppearance.qml:139
#, kde-format
msgid "Change Regional Settings…"
msgstr "Breyta svæðisstillingum…"

#: package/contents/ui/configAppearance.qml:150
#, kde-format
msgid "Date format:"
msgstr "Snið dagsetninga:"

#: package/contents/ui/configAppearance.qml:158
#, kde-format
msgid "Long Date"
msgstr "Löng dagsetning"

#: package/contents/ui/configAppearance.qml:163
#, kde-format
msgid "Short Date"
msgstr "Stutt dagsetning"

#: package/contents/ui/configAppearance.qml:168
#, kde-format
msgid "ISO Date"
msgstr "ISO-dagsetning"

#: package/contents/ui/configAppearance.qml:173
#, kde-format
msgctxt "custom date format"
msgid "Custom"
msgstr "Sérstillt"

#: package/contents/ui/configAppearance.qml:204
#, kde-format
msgid ""
"<a href=\"https://doc.qt.io/qt-5/qml-qtqml-qt.html#formatDateTime-method"
"\">Time Format Documentation</a>"
msgstr ""
"<a href=\"https://doc.qt.io/qt-5/qml-qtqml-qt.html#formatDateTime-method"
"\">Hjálparskjöl fyrir tímasnið</a>"

#: package/contents/ui/configAppearance.qml:228
#, kde-format
msgctxt "@label:group"
msgid "Text display:"
msgstr "Birting texta:"

#: package/contents/ui/configAppearance.qml:230
#, kde-format
msgctxt "@option:radio"
msgid "Automatic"
msgstr "Sjálfvirkt"

#: package/contents/ui/configAppearance.qml:234
#, kde-format
msgctxt "@label"
msgid ""
"Text will follow the system font and expand to fill the available space."
msgstr "Textinn notar leturgerð kerfis og fyllir út í tiltækt pláss."

#: package/contents/ui/configAppearance.qml:243
#, kde-format
msgctxt "@option:radio setting for manually configuring the font settings"
msgid "Manual"
msgstr "Handvirkt"

#: package/contents/ui/configAppearance.qml:253
#, kde-format
msgctxt "@action:button"
msgid "Choose Style…"
msgstr "Veldu stíl…"

#: package/contents/ui/configAppearance.qml:266
#, kde-format
msgctxt "@info %1 is the font size, %2 is the font family"
msgid "%1pt %2"
msgstr "%1pt %2"

#: package/contents/ui/configAppearance.qml:277
#, kde-format
msgctxt "@title:window"
msgid "Choose a Font"
msgstr "Veldu leturgerð"

#: package/contents/ui/configCalendar.qml:39
#, kde-format
msgid "General:"
msgstr "Almennt:"

#: package/contents/ui/configCalendar.qml:40
#, kde-format
msgid "Show week numbers"
msgstr "Sýna vikunúmer"

#: package/contents/ui/configCalendar.qml:45
#, kde-format
msgid "First day of week:"
msgstr "Fyrsti dagur viku:"

#: package/contents/ui/configCalendar.qml:68
#, kde-format
msgid "Available Plugins:"
msgstr "Tiltækar tengiviðbætur:"

#: package/contents/ui/configTimeZones.qml:39
#, kde-format
msgid ""
"Tip: if you travel frequently, add another entry for your home time zone to "
"this list. It will only appear when you change the systemwide time zone to "
"something else."
msgstr ""

#: package/contents/ui/configTimeZones.qml:101
#, kde-format
msgid "Clock is currently using this time zone"
msgstr "Klukkan er að nota þetta tímabelti"

#: package/contents/ui/configTimeZones.qml:103
#, kde-format
msgctxt ""
"@label This list item shows a time zone city name that is identical to the "
"local time zone's city, and will be hidden in the timezone display in the "
"plasmoid's popup"
msgid "Hidden while this is the local time zone's city"
msgstr ""

#: package/contents/ui/configTimeZones.qml:117
#, fuzzy, kde-format
#| msgid "Switch Local Time Zone…"
msgid "Switch Systemwide Time Zone…"
msgstr "Skipta um staðbundið tímabelti…"

#: package/contents/ui/configTimeZones.qml:126
#, kde-format
msgid "Remove this time zone"
msgstr "Fjarlægja þetta tímabelti"

#: package/contents/ui/configTimeZones.qml:135
#, fuzzy, kde-format
#| msgid "System's Local Time Zone"
msgid "Systemwide Time Zone"
msgstr "Staðbundið tímabelti kerfisins"

#: package/contents/ui/configTimeZones.qml:135
#, kde-format
msgid "Additional Time Zones"
msgstr "Viðbótartímabelti"

#: package/contents/ui/configTimeZones.qml:148
#, kde-format
msgid ""
"Add more time zones to display all of them in the applet's pop-up, or use "
"one of them for the clock itself"
msgstr ""
"Bættu við fleiri tímabeltum til að birta þau öll í sprettiglugga "
"smáforritsins, eða notaðu eitt þeirra fyrir klukkuna sjálfa"

#: package/contents/ui/configTimeZones.qml:155
#, kde-format
msgid "Add Time Zones…"
msgstr "Bæta við tímabeltum…"

#: package/contents/ui/configTimeZones.qml:165
#, kde-format
msgid "Switch displayed time zone by scrolling over clock applet"
msgstr "Skiptu um birt tímabelti með því að fletta yfir klukkuforritið"

#: package/contents/ui/configTimeZones.qml:172
#, fuzzy, kde-format
#| msgid ""
#| "Note that using a different time zone for the clock does not change the "
#| "systemwide local time zone. When you travel, switch the local time zone "
#| "instead."
msgid ""
"Using this feature does not change the systemwide time zone. When you "
"travel, switch the systemwide time zone instead."
msgstr ""
"Athugaðu að þegar klukkan er stillt á annað tímabelti breytir það ekki "
"staðbundnu tímabelti fyrir allt kerfið. Þegar þú ferðast skaltu breyta "
"staðbundna tímabeltinu í staðinn."

#: package/contents/ui/configTimeZones.qml:198
#, kde-format
msgid "Add More Timezones"
msgstr "Bæta við fleiri tímabeltum"

#: package/contents/ui/configTimeZones.qml:209
#, kde-format
msgid ""
"At least one time zone needs to be enabled. Your local timezone was enabled "
"automatically."
msgstr ""
"Að minnsta kosti eitt tímabelti þarf að vera virkjað. Staðbundna tímabeltið "
"þitt var virkjað sjálfkrafa."

#: package/contents/ui/configTimeZones.qml:234
#, kde-format
msgid "%1, %2 (%3)"
msgstr "%1, %2 (%3)"

#: package/contents/ui/configTimeZones.qml:234
#, kde-format
msgid "%1, %2"
msgstr "%1, %2"

#: package/contents/ui/main.qml:134
#, kde-format
msgid "Copy to Clipboard"
msgstr "Afrita á klippispjald"

#: package/contents/ui/main.qml:139
#, kde-format
msgid "Adjust Date and Time…"
msgstr "Stilla tíma og dagsetningu...…"

#: package/contents/ui/main.qml:142
#, kde-format
msgid "Set Time Format…"
msgstr "Stilla tímasnið…"

#: package/contents/ui/Tooltip.qml:33
#, kde-format
msgctxt "@info:tooltip %1 is a localized long date"
msgid "Today is %1"
msgstr "Í dag er %1"

#: package/contents/ui/Tooltip.qml:113
#, kde-format
msgctxt "@label %1 is a city or time zone name"
msgid "%1:"
msgstr ""

#: plugin/clipboardmenu.cpp:110
#, kde-format
msgid "Other Calendars"
msgstr "Önnur dagatöl"

#: plugin/clipboardmenu.cpp:118
#, kde-format
msgctxt "unix timestamp (seconds since 1.1.1970)"
msgid "%1 (UNIX Time)"
msgstr "%1 (UNIX-tími)"

#: plugin/clipboardmenu.cpp:121
#, kde-format
msgctxt "for astronomers (days and decimals since ~7000 years ago)"
msgid "%1 (Julian Date)"
msgstr "%1 (Júliönsk dagsetning)"

#: plugin/timezonemodel.cpp:141
#, kde-format
msgctxt "This means \"Local Timezone\""
msgid "Local"
msgstr "Staðbundið"

#: plugin/timezonemodel.cpp:143
#, kde-format
msgid "System's local time zone"
msgstr "Staðbundið tímabelti kerfisins"

#~ msgctxt "Format: month year"
#~ msgid "%1 %2"
#~ msgstr "%1 %2"

#~ msgid "Keep Open"
#~ msgstr "Halda opnu"

#~ msgid "Days"
#~ msgstr "Dagar"

#~ msgid "Months"
#~ msgstr "Mánuðir"

#~ msgid "Years"
#~ msgstr "Ár"

#~ msgid "Previous month"
#~ msgstr "Fyrri mánuður"

#~ msgid "Previous year"
#~ msgstr "Fyrra ár"

#~ msgid "Previous decade"
#~ msgstr "Fyrri áratugur"

#~ msgctxt "Reset calendar to today"
#~ msgid "Today"
#~ msgstr "Í dag"

#~ msgid "Reset calendar to today"
#~ msgstr "Endurstilla dagatal á daginn í dag"

#~ msgid "Next month"
#~ msgstr "Næsti mánuður"

#~ msgid "Next year"
#~ msgstr "Næsta ár"

#~ msgid "Next decade"
#~ msgstr "Næsti áratugur"
